import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { FlatList, Button, TextInput, StyleSheet, Text, View } from 'react-native';


	
const sampleGoals = [
  "Faire les courses",
  "Aller à la salle de sport 3 fois par semaine",
  "Monter à plus de 5000m d altitude",
  "Acheter mon premier appartement",
  "Perdre 5 kgs",
  "Gagner en productivité",
  "Apprendre un nouveau langage",
  "Faire une mission en freelance",
  "Organiser un meetup autour de la tech",
  "Faire un triathlon",
];





function addGoal(newGoal) {
  sampleGoals.push(newGoal);
}

export default function App() {

  const newGoal = useState();
  var [sampleGoal, deleteGoal] = React.useState(sampleGoals);
  
  deleteGoal = (Goal) => {
    var index = sampleGoal.indexOf(Goal);
    sampleGoal.splice(index, 1);
    console.log(Goal);
  }

  const _renderItem = ({ item }) => <View style={styles.row}><Text>{item}  </Text><Button value={item} color="red" title="X" onPress={() => deleteGoal(item)}/></View>

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Open up <Text style={styles.bold}>App.js</Text> to start working on your app!</Text>
      <FlatList data={sampleGoal} renderItem={_renderItem}>
      </FlatList>
      <TextInput style={styles.input} onChangeText={(text) => this.setState({newGoal: text})}/>
      <Button title="Add" onPress={addGoal(newGoal)}/>
      <StatusBar style="auto"/>      
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: 'red',
    fontSize: 20,
  },
  bold: {
    fontWeight: 'bold',
  },
  input: {
    width: 200,
    borderWidth: 1,
  },
  delete: {
    backgroundColor: 'red',
    width: 40,
  },
  row: {
    flexDirection: 'row',
  }
});
